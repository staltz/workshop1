function filter(condition) {
  return function*(inputIter) {
    for (let x of inputIter) {
      if (condition(x)) yield x;
    }
  };
}

function* range(from, to) {
  let x = from;
  while (x <= to) {
    yield x;
    x += 1;
  }
}

function isOdd(x) {
  return x % 2 === 1;
}

for (let x of filter(isOdd)(range(10, 30))) {
  console.log(x);
}
