function manySS(cb) {
  cb(10);
  cb(10);
  cb(10);
  cb(10);
}

function randomSS(cb) {
  cb(Math.random());
}

function map(f) {
  return inSS => function outSS(cb) {
    inSS(x => cb(f(x)))
  }
}

const tenTimesRandomSS = map(x => x * 10)(randomSS);

manySS(x => console.log('many', x));
randomSS(x => console.log('random', x));
tenTimesRandomSS(x => console.log('tenTimesRandom', x));
