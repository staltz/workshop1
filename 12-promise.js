/**
 * A promise is:
 *
 * A setter of two setters, plus a guarantee:
 * only ONE of those setters will be called, if any.
 *
 * It's inversion of control with a contract
 * that limits the scope of that "control".
 */

function later(value) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value);
    }, 1000);
  });
}

later('hello')
  .then(x => later(x.toUpperCase()))
  .then(console.log, console.error);
