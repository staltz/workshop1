// Works in Firefox 57+, Chrome 70+

function* range(from, to) {
  let x = from;
  while (x <= to) {
    yield x;
    x += 1;
  }
}

function map(f) {
  return async function*(inputAsyncIter) {
    for await (let x of inputAsyncIter) {
      yield f(x);
    }
  };
}

function map(f) {
  return async function*(inputAsyncIter) {
    for await (let x of inputAsyncIter) {
      yield await f(x);
    }
  };
}

function filter(g) {
  return async function*(inputAsyncIter) {
    for await (let x of inputAsyncIter) {
      if (g(x)) yield x;
    }
  };
}

function pipe(...fns) {
  let x = fns[0];
  for (let i = 1; i < fns.length; i++) {
    x = fns[i](x);
  }
  return x;
}

async function main() {
  const names = pipe(
    range(1, 10),
    map(i => `http://jsonplaceholder.typicode.com/users/${i}`),
    map(fetch),
    map(res => res.json()),
    filter(user => user.address.geo.lat < 0),
    map(user => user.name),
  );

  for await (let name of names) {
    console.log(name);
  }
}

main();
