/**
 * Exercise: implement range() as a getter getter of numbers.
 */

// Produces the sequence of numbers: from, from+1, from+2, ..., to
function range(from, to) {
  return () => {
    let x = from;
    return () => {
      try {
        return x;
      } finally {
        x = x + 1;
        if (x > to) x = undefined;
      }
    };
  };
}

const getter = range(10, 30);
for (let get = getter(), x = get(); !!x; x = get()) {
  console.log(x);
}
