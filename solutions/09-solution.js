function map(transform) {
  return function*(inputIter) {
    for (let x of inputIter) {
      yield transform(x);
    }
  };
}

function skip(n) {
  return function*(inputIter) {
    let i = 0;
    for (let x of inputIter) {
      if (++i <= n) continue;
      yield x;
    }
  };
}

function take(n) {
  return function*(inputIter) {
    let i = 0;
    for (let x of inputIter) {
      if (i >= n) return;
      yield x;
      i += 1;
    }
  };
}

function filter(condition) {
  return function*(inputIter) {
    for (let x of inputIter) {
      if (condition(x)) yield x;
    }
  };
}

function* range(from, to) {
  let x = from;
  while (x <= to) {
    yield x;
    x += 1;
  }
}

function pipe(...fns) {
  let x = fns[0];
  for (let i = 1; i < fns.length; i++) {
    x = fns[i](x);
  }
  return x;
}

const numbers = pipe(
  range(10, 30),
  filter(x => x % 2 === 1),
  map(x => x * 2),
  skip(1),
  take(4),
);

const expected = [26, 30, 34, 38];
for (let x of numbers) {
  const e = expected.shift();
  if (x === e) {
    console.log(x);
  } else {
    console.error(`:( expected ${e}, got ${x}`);
    process.exit(1);
  }
}
console.log(':) passed!');
process.exit(0);
