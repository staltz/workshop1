function later(value) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value);
    }, 1000);
  });
}

async function* random() {
  let n = 5;
  while (--n >= 0) {
    yield await later(Math.random());
  }
}

(async function main() {
  for await (let x of random()) {
    console.log(x);
  }
})()
