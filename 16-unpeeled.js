function later(value) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value);
    }, 1000);
  });
}

/**
 * An AsyncIterable is a getter-getter of Promises, plus
 * completion markers in the form of {done, value} objects.
 */

function range(from, to) {
  return {
    [Symbol.asyncIterator]: () => {
      let x = from;
      return {
        next: () => {
          if (x > to) return Promise.resolve({done: true});
          else {
            return later(x++).then(value => {
              return {done: false, value};
            });
          }
        },
      };
    },
  };
}

async function main() {
  for await (let x of range(10, 30)) {
    console.log(x);
  }
}

main();
