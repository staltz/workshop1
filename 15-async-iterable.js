function later(value) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value);
    }, 1000);
  });
}

/**
 * An AsyncIterable is a getter-getter of Promises, plus
 * completion markers in the form of {done, value} objects.
 */

async function* range(from, to) {
  let x = from;
  while (x <= to) {
    yield await later(x);
    x += 1;
  }
}

async function main() {
  for await (let x of range(10,30)) {
    console.log(x);
  }
}

main();
