/**
 * Takes an input iterable and returns an iterable
 * where each element was passed through `transform`,
 * like in Array.map.
 *
 * Example:
 * Input iterable delivers 1...2...3...4...5...
 * transform is x => x * 10
 * Output iterable delivers 10...20...30...40...50...
 */
function map(transform) {
  // ?
}

/**
 * Takes an input iterable and returns an iterable that
 * ignores the first n elements, but delivers the rest.
 *
 * Example:
 * Input iterable delivers 1...2...3...4...5...
 * n = 3
 * Output iterable delivers 4...5...
 */
function skip(n) {
  // ?
}

/**
 * Takes an input iterable and returns an iterable
 * that delivers only the first n elements.
 *
 * Example:
 * Input iterable delivers 1...2...3...4...5...
 * n = 3
 * Output iterable delivers 1...2...3
 */
function take(n) {
  // ?
}

function filter(condition) {
  return function*(inputIter) {
    for (let x of inputIter) {
      if (condition(x)) yield x;
    }
  };
}

function* range(from, to) {
  let x = from;
  while (x <= to) {
    yield x;
    x += 1;
  }
}

function pipe(...fns) {
  let x = fns[0];
  for (let i = 1; i < fns.length; i++) {
    x = fns[i](x);
  }
  return x;
}

const numbers = pipe(
  range(10, 30),
  filter(x => x % 2 === 1),
  map(x => x * 2),
  skip(1),
  take(4),
);

const expected = [26, 30, 34, 38];
for (let x of numbers) {
  const e = expected.shift();
  if (x === e) {
    console.log(x);
  } else {
    console.error(`:( expected ${e}, got ${x}`);
    process.exit(1);
  }
}
console.log(':) passed!');
process.exit(0);
