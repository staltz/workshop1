// Works in Firefox 57+, Chrome 70+

async function main() {
  /**
   * EXERCISE: implement and consume an async iterable
   * to fetch user data from jsonplaceholder
   * in the range
   * http://jsonplaceholder.typicode.com/users/${from}
   * to
   * http://jsonplaceholder.typicode.com/users/${to}
   */
}

main();
