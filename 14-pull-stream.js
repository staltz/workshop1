/**
 * A pull-stream source is:
 *
 * A setter-setter, plus a guarantee:
 * the inner setter will be called at most once
 * for each call of the outer setter.
 *
 * It's inversion of control with a contract
 * that limits the scope of that "control".
 *
 * The pull-stream can be also seen as a getter
 * of an (async) value.
 */

let n = 5;
function random(end, cb) {
  if (end) return cb(end);
  if (0 > --n) return cb(true);
  setTimeout(() => {
    cb(null, Math.random());
  }, 1000);
}

/**
 * A pull-stream sink is a setter of a pull-stream source.
 *
 * Hence: setter-setter-setter :)
 */

function logger (read) {
  read(null, function next(end, data) {
    if(end === true) return
    if(end) throw end

    console.log(data)
    read(null, next)
  })
}

logger(random)