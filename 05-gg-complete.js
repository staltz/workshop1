function range(from, to) {
  return () => {
    let x = from;
    return () => {
      if (x > to) return {done: true};
      else return {done: false, value: x++};
    }
  }
}

const gg = range(10, 30);
const g = gg();

for (let res = g(); !res.done; res = g()) {
  console.log(res.value);
}
