function later(value) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value);
    }, 1000);
  });
}

async function main() {
  const x = await later('hello');
  const y = await later(x.toUpperCase());
  console.log(y);
}

main();
