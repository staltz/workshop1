## Schedule

### Part 1

- Lossy vs lossless abstractions
- Weak vs strong abstractions
- Function as an abstraction
- Getters (01, 02)
- Getter-getters (03, 04, 05)
- Iterables (06, 07, 08, 09)

### (break)

### Part 2

- Setters (10)
- Setter-setters (11)
- Promises (12, 13)
- Pull-streams (14)
- AsyncIterables (15, 16, 17, 18, 19)

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a>