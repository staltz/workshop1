function range(from, to) {
  return {
    [Symbol.iterator]: () => {
      let x = from;
      return {
        next: () => {
          if (x > to) return {done: true};
          else return {done: false, value: x++};
        },
      };
    },
  };
}

for (let x of range(10, 30)) {
  console.log(x);
}
