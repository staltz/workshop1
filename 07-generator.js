function* range(from, to) {
  let x = from;
  while (x <= to) {
    yield x;
    x += 1;
  }
}

for (let x of range(10,30)) {
  console.log(x);
}
